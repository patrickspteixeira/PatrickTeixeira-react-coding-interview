import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IPerson } from '@lib/models/person';
import ContactTitleEdit, { ContactFormValues } from './contactTitleEdit';

export type OnEditParams = {
  id: string
  email: string
} & ContactFormValues

export interface IContactCardProps {
  person: IPerson;
  onEdit(params: OnEditParams): void;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { id, firstName, lastName, email },
  onEdit,
  sx
}) => {
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar sx={{ mr: 1.5 }} />
        <Box textAlign="center" mt={2}>
          <ContactTitleEdit firstName={firstName} lastName={lastName} onSubmit={values => onEdit({id, ...values, email})} />
          <Typography variant="caption" color="text.secondary">
            {email}
          </Typography>
        </Box>
      </Box>
    </Card>
  );
};
