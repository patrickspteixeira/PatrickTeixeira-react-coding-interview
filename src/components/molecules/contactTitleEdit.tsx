import { Edit } from '@mui/icons-material'
import { Button, IconButton, TextField, Typography } from '@mui/material'
import { FormEvent, useState } from 'react'

export type ContactFormValues = {
    firstName: string
    lastName: string
}

type Props = {
    firstName: string
    lastName: string
    onSubmit: (values: ContactFormValues) => void
}

function ContactTitleEdit({firstName, lastName, onSubmit}: Props) {
  const [isLocalEditing, setIsLocalEditing] = useState(false)
  const [first, setFirst] = useState(firstName)
  const [last, setLast] = useState(lastName)

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (first && last) {
        try {
            await onSubmit({firstName: first, lastName: last})
            setIsLocalEditing(false)
        } catch (error) {
            console.log('There was an error updating the user.')
        }
    }
  }

  if (isLocalEditing) {
    return (
        <form onSubmit={handleSubmit} noValidate>
            <TextField onChange={event => setFirst(event.target.value)} label="First Name" value={first} />
            <TextField onChange={event => setLast(event.target.value)} label="Last Name" value={last} />
            <Button type='submit'>Submit</Button>
        </form>
    )
  }

  return (
    <Typography variant="subtitle1" lineHeight="1rem">
     {first} {last} <IconButton onClick={() => setIsLocalEditing(true)} size='small' aria-label='edit contact name'><Edit fontSize='inherit' /></IconButton>
    </Typography>
  )
}

export default ContactTitleEdit